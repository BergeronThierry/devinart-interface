import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './views/components/Header';


import { create } from "./state/store";


const testObject = {
    key1: 1,
    key2: 2
}
create(testObject);



function App() {
    return (
        <div className="App">
            <Router>
                <Header />
                <Switch>
                    <Route to="/">

                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
