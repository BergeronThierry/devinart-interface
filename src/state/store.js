class Store {
    static instance;
    constructor() {
        if (Store.instance) {
            return Store.instance
        }
        this.store = {};
        this.subscriptions = {};
        this.subs = new Set();
        Store.instance = this;
    }
    setState = (key, value) => {
        this.store[key] = value;
        if(!this.subscriptions[key]){
            this.subscriptions[key] = new Set();
        }
        else{
            this.update(key);
        }
    }
    getState = (key) => {
        if (!key) {
            return this.store;
        }
        return this.store[key];
    }
    subscribe = (fn, key = undefined) => {
        if(!key){
            this.subs.add(fn);
        }
        this.subscriptions[key].add(fn);
    }
    unsubscribe = (fn, key = undefined) => {
        if(! key){
            this.subs.delete(fn);
            return;
        }
        this.subscriptions[key].delete(fn);
    }
    update = (key = undefined) =>{
        if(key){
            this.subscriptions[key].forEach(fn => fn());
        }
    }
}
export default new Store();