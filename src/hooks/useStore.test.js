import { useStore } from "./useStore";
import  store  from "../state/store";

test('testing useStore', () => {
    const [value, setValue] = useStore('test');

    expect(value).toBe(undefined);
    setValue(123);

    expect(value).toBe(123);
    setValue(321)

    expect(value).toBe(321);
})