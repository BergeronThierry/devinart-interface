/**
 * State management wrapper
 */
import store from "../state/store";
import { useEffect, useState } from "react";

export const useStore = (key, value) => {
    const [state, setState] = useState(store.getState(key));
    store.setState(key, value);

    const updateState = (value) => {
        store.setState(value);
    }

    useEffect(() => {
        const update = () =>{
            const newValue = store.getState(key);
            setState(newValue);
        }
        store.subscribe(update, key);

        return () => {
            store.unsubscribe(update, key);
        }
    }, [])

    return [
        state,
        updateState
    ]
}